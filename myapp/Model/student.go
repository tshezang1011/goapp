package Model

import (
	"myapp/dataStore/postgres"
)

type Student struct {
	StdId     int64  `json:"stdid"`
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	Email     string `json:"email"`
}

const queryInsertUser = "INSERT INTO student(stdid, firstname, lastname, email) VALUES ($1, $2, $3, $4);"

func (s *Student) Create() error {
	_, err := postgres.Db.Exec(queryInsertUser, s.StdId, s.FirstName, s.LastName, s.Email)

	return err
}

var queryGetUser = "SELECT stdid, firstname, lastname, email FROM student WHERE stdid=$1;"

func (s *Student) Read() error {
	return postgres.Db.QueryRow(queryGetUser, s.StdId).Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
}

var queryUpdateUser = "UPDATE student SET stdid = $1, firstname = $2, lastname=$3, email=$4 WHERE stdid=$5;"

func (s *Student) Update(oldID int64) error {
	_, err := postgres.Db.Exec(queryUpdateUser, s.StdId, s.FirstName, s.LastName, s.Email, oldID)
	return err
}

var queryDeleteUser = "DELETE FROM student WHERE stdid=$1;"

func (s *Student) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteUser, s.StdId); err != nil {
		return err
	}
	return nil
}

func GetAllStudents() ([]Student, error) {
	rows, getErr := postgres.Db.Query("SELECT * FROM student;")
	if getErr != nil {
		return nil, getErr
	}
	students := []Student{}

	for rows.Next() {
		var s Student
		dbErr := rows.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
		if dbErr != nil {
			return nil, dbErr
		}
		students = append(students, s)
	}
	rows.Close()
	return students, nil
}
