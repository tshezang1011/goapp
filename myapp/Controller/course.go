package Controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"myapp/Model"
	"myapp/utils/httpResp"
	"net/http"

	"github.com/gorilla/mux"
)

func Addcourse(w http.ResponseWriter, r *http.Request) {
	var cour Model.Course
	_ = cour

	decoder := json.NewDecoder(r.Body)
	// fmt.Println(decoder)

	if err := decoder.Decode(&cour); err != nil {

		response, _ := json.Marshal(map[string]string{"error": "Invalid Json Type"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	defer r.Body.Close()

	saveErr := cour.Create()
	fmt.Println(saveErr)
	if saveErr != nil {
		response, _ := json.Marshal(map[string]string{"error": saveErr.Error()})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}
	//no error
	response, _ := json.Marshal(map[string]string{"status": "course added"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func Getcour(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	fmt.Println(cid)
	courseid, idErr := getCourseId(cid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	c := Model.Course{Courseid: courseid}
	getErr := c.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.RespondWithError(w, http.StatusNotFound, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, c)
}

func getCourseId(courseIdParam string) (string, error) {
	return courseIdParam, nil
}

// func getCourseId(courseIdParam string) (int64, error) {
// 	courseId, courseErr := strconv.ParseInt(courseIdParam, 10, 64)
// 	if courseErr != nil {
// 		return 0, courseErr
// 	}
// 	return courseId, nil
// }

func UpdateCourse(w http.ResponseWriter, r *http.Request) {
	old_cid := mux.Vars(r)["cid"]
	old_courseId, idErr := getCourseId(old_cid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var cours Model.Course
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&cours); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid jason body")
		return
	}
	defer r.Body.Close()

	err := cours.Update(old_courseId)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, cours)

}

func DeleteCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	courseid, idErr := getCourseId(cid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	c := Model.Course{Courseid: courseid}
	if err := c.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, getErr := Model.GetAllCourses()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, courses)
}
